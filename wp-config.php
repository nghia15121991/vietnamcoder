<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_vietnamcoder');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-G@D hfo3Il*I750<j4IcS&W?TZ6g~9s$%Q%CT@%/PP+,*t$_Qy?-`wtFX>=XKkf');
define('SECURE_AUTH_KEY',  'c+%vtj|.Vc2J+KWs?&Rl-3PS!,b5F@bGP+5O^.UaXie?vky AiKoD]57F76ArY{@');
define('LOGGED_IN_KEY',    'JPc*Z.[l04J(iOM*5w*Y}`o2z[ o`D<SQ8ajz^u 5F,*fYvx1#?~>QvqJ,)IsvP|');
define('NONCE_KEY',        'IaMKYB7&l+Oz=zM~qdC(_h-I<uLg}kg.Np*xsEceqjdv1T(P&{Ixnr$tMYa`1[,5');
define('AUTH_SALT',        'mUI(|0sMKAUu*rU^+fd{MzrI rN941Tq-AWS o/cmRi2hmz0A8r_zS:WcXN@>$9v');
define('SECURE_AUTH_SALT', 'P<uPP}Pemk4tOXs9k,I;5^c$El:@p#>uW6CsH$z-HtvCdw)^J6fjf`_gH/2>O@wv');
define('LOGGED_IN_SALT',   'JD}zk*9V~=R{Mq9}:$V^^bN7NDX}L s&r*}U8~l<mrdPa^!z:P5B`90*iOUb0Rmv');
define('NONCE_SALT',       'rZId*5-u,Cn|#1<>Q6R!Nfr/Ov~E5DK($A;LhC[#7JqJrm$4JS{nfn=A.F$0J0il');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'dn_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
