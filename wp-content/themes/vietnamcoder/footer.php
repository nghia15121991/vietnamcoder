
</section>
<footer>
    <div class="top-footer">
        <div class="dn-container">
            <div class="dn-flex footer-column">
                <div class="left-content-footer">
                    <ul class="dn-flex">
                        <li><a href="http://">Contact us</a></li>
                        <li><a href="http://">About</a></li>
                        <li><a href="http://">Q&A</a></li>
                        <li><a href="http://">Policy</a></li>
                    </ul>
                </div>
                <div class="right-content-footer">
                    <form action="" method="post">
                        <label for="email">Nhận bài viết mới nhất:</label>
                        <input type="text" name="email" id="email" placeholder="Nhập địa chỉ email">
                        <input type="submit" value="Đăng ký">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="bot-footer">
        <div class="dn-container">
            <div class="dn-flex footer-column">
                <div class="right-content-footer">
                    <span>Copyright © 2018 by Dinh Nghia. Proudly powered by WordPress</span>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer();?>
</body>
</html>