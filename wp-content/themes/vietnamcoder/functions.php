<?php


// Define

require_once("inc/setup.php");
require_once("inc/menu/main-menu.php");
require_once("inc/widgets/class-vnc-widget-recent-posts.php");
require_once("inc/action/hook.php");
require_once("inc/action/functions_hook.php");


function dn_posted_meta()
{
    $posted_on = human_time_diff( get_the_time('U') , current_time('timestamp') );	
	return '<span class="posted-on"><a href="'. esc_url( get_permalink() ) .'">' . $posted_on . ' trước</a></span>';
}

function dn_posted_tag()
{
	return  get_the_tag_list('<div class="tag-box">', '', '</div>');
}