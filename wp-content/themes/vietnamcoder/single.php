<?php get_header() ?>

<div class="dn-container">
    <div class="article">
        <div class="content-left">
            <?php 
            if (have_posts()):?>
                    <?php
                        while(have_posts()): the_post()?>     
                                    <div class="arcticle-top-box">
                                        <div class="title-box">
                                            <?php the_title('<h2>','</h2>');?>
                                        </div>
                                        <div class="time-box">
                                            <?php echo dn_posted_meta(); ?>
                                            <span class="entry-comment"><?php comments_popup_link( __('0 Bình luận','vietnamcoder'), __('1 Bình luận', 'vietnamcoder'), __('% Bình luận', 'vietnamcoder'), 'comments-link', __('Tắt Bình luận', 'vietnamcoder'));?></span>
                                        </div>
                                    </div>
                                    <div class="content-box">
                                        <?php the_content(); ?>
                                    </div>
                                        <?php echo dn_posted_tag(); ?>        
                <?php	
                        endwhile;
                    else:
                        echo "Hiện tại chưa có bài viết nào" 
                ?>
            <?php 
            endif;
            ?>
        </div>
        <div class="content-right">
            <div class="content-right-top">
            </div>
            <?php get_sidebar('default'); ?>
        </div>
    </div>
    <div class="content-left">
        <div>
            <?php
            if (is_active_sidebar( 'sidebar-2' ) ):?>
                <aside>
                    <?php dynamic_sidebar( 'sidebar-2' ); ?>
                </aside><!-- #secondary -->
            <?php
            endif;
            ?>

        </div>
        <?php
        // Nếu bình luận được cho phép thì sẽ mở bình luận
        if ( comments_open()) :
            comments_template();
        endif; 
        ?>
    </div>
    
</div><!-- #main-content -->

<?php
get_footer();