<?php
/**
 * Trang đầu tiên vào website
 *
 *
 *
 * @package WordPress
 * @subpackage vietnamcoder
 * @since 1.0
 * @version 1.0
 */

get_header();?>
        <div class="dn-container slide-section-phone">
            <div class="dn-flex dn-wrap-full container-box">
            <?php
                $category = get_queried_object();
                $query = array(
                    'post_type'     =>  'post',
                    'post_status'   =>  'publish',
                    'cat'           =>  $category->term_id
                );
                
                $posts = new WP_Query($query);
                if ($posts->have_posts()):
                    while($posts->have_posts()): $posts->the_post()?>     
                        <div class="dn-box width-3">
                            <div class="top-box">
                                <div class="img-box">
                                    <?php the_post_thumbnail('medium')?>
                                </div>
                            </div>
                            <div class="middle-box">
                                <div>
                                    <div class="title-box">
                                        <?php the_title('<h2>','</h2>');?>
                                    </div>
                                    <div class="time-box">
                                        <?php echo dn_posted_meta(); ?>
                                        <span class="entry-comment"><?php comments_popup_link( __('0 Bình luận','vietnamcoder'), __('1 Bình luận', 'vietnamcoder'), __('% Bình luận', 'vietnamcoder'), 'comments-link', __('Tắt Bình luận', 'vietnamcoder'));?></span>
                                    </div>
                                </div>
                                <div class="content-box">
                                    <?php the_excerpt(); ?>
                                </div>
                                    <?php echo dn_posted_tag(); ?>
                            </div>
                            <div class="bot-box">
                                <div class="detail-box">
                                    <a href="<?php the_permalink();?>">Xem thêm</a>
                                </div>
                                
                            </div>
                        </div>
            <?php
                    endwhile;
                else:
                    echo "Hiện tại chưa có bài viết nào" 
            ?>
            <?php 
                endif;
            ?>

            </div>
        </div>
<?php 
get_footer();