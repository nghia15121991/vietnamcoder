(function( $ ) {
	$('.menu-phone span').on('click',function () {
        $('.nav-menu-phone').animate({
            width: "toggle"
        }
        );
        $('body').toggleClass("disable-overflow");
        $('.drawer-backdrop').toggleClass("drawer-backdrop-show");
    });
    $('.header-container .icon-search').on('click', function () {
        $('.header-container .form-search').animate({
            width: "toggle"
        });
    });
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 80) {
            $('.header-bottom').addClass('nav-fixed');
        } else {
            $('.header-bottom').removeClass('nav-fixed');
        }
    })
    //Tăng kích thước theo height lớn nhất
    var heights = $(".recent-post-title").map(function ()
    {
        return $(this).height();
    }).get();

    maxHeight = Math.max.apply(null, heights);

    $('.recent-post-title').height(maxHeight);
})( jQuery );
