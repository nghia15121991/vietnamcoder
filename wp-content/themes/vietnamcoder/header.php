<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header>
    <div class="header-top">
        <div class="dn-container">
            <div class="dn-flex dn-column-wrapper">
                <div class="left-content">
                    <ul class="dn-flex">
                        <li>
                            <span class="icon-phone1"></span>
                            <span>0378780208</span> 
                        </li>
                        <li>
                            <span class="icon-mail-envelope-closed"></span>
                            <span>nghia15121991@gmail.com</span>
                            
                        </li>
                    </ul>
                </div>
                <div class="right-content">
                    <div>
                        <ul class="dn-flex social-menu">
                            <li><a href="http://" target="_blank"><span class="icon-facebook"></span></a></li>
                            <li><a href="http://" target="_blank"><span class="icon-googleplus"></span></a></li>
                            <li><a href="http://" target="_blank"><span class="icon-linkedin2"></span></a></li>
                            <li><a href="http://" target="_blank"><span class="icon-twitter1"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="dn-container header-container">
            <div class="dn-flex dn-column-wrapper">
                <div class="logo">
                    <a href="<?php echo get_home_url();?>">
                        <span class="name-logo">Việt Nam Coder</span>
                        <span class="slogan">Chia sẻ kiến thức lập trình và đời sống</span>
                    </a>
                </div>
                <div class="menu-phone">
                    <div>
                        <span class="icon-menu1"></span>
                    </div>
                    
                </div>
                <div class="menu-navigation">
                    <?php
                        wp_nav_menu(
                            array(
                            'theme_location' => 'main-header',
                            'menu_class' => 'dn-flex',
                            'container' => '',
                            'walker' => new Bootstrap_Nav_Walker
                        ));
                    ?>
                </div>
                <div class="icon-search">
                </div>
                <div class="nav-menu-phone">
                </div>
            </div>
            <div class="form-search">
                <form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<input type="search" class="search-field" placeholder="Tìm kiếm sản phẩm" value="<?php echo get_search_query(); ?>" name="s" autocomplete="off" />
					<input type="hidden" name="post_type" value="post" />
				</form>
            </div>
        </div>
    </div>

</header>

<section>

<div class="drawer-backdrop">

</div>