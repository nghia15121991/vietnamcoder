<?php
/**
 * Danh sách các function callback
 *
 *
 *
 * @package WordPress
 * @subpackage vietnamcoder
 * @since 1.0
 * @version 1.0
 */
function aego_themes_css()
{
    //Add css
    // wp_register_style('bootstrap.css', get_template_directory_uri().'/assets/wildcard/boostrap//bootstrap.min.css');
    // $dependencies = array('bootstrap');
    // wp_enqueue_style('bootstrap.css', $dependencies);
    wp_enqueue_style('style',get_template_directory_uri().'/assets/myself/css/custom.css');
    wp_enqueue_style('my-font',get_template_directory_uri().'/assets/myself/css/my-font.css');
}
function aego_themes_js()
{
    //Add js
    if ( !is_admin() ) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', '3.3.1', true );
        wp_enqueue_script('jquery');
    }
    // wp_enqueue_script('bootstrap.js', get_template_directory_uri().'/assets/dist/js/bootstrap.min.js', array('jquery'), '3.7.0', true );
    wp_enqueue_script('custom', get_template_directory_uri().'/assets/myself/js/custom.js' , array('jquery'), '1.0.0', true );
    // wp_enqueue_script('my-font', get_template_directory_uri().'/assets/myself/js/my-font.js' , array('jquery'), '1.0.0', true );
}

function my_deregister_scripts(){
    wp_dequeue_script( 'wp-embed' );
}



if (!function_exists('initialize_widget')) {
    function initialize_widget()
    {
        register_widget('VNC_Widget_Recent_Posts');
        register_sidebar( array(
            'name'          => __( 'Thanh bên website', 'vietnamcoder' ),
            'id'            => 'sidebar-1',
            'description'   => __( 'Thêm widgets ở đây sẽ xuất hiện trong bài viết cho blog', 'vietnamcoder' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s footer-column">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        ) );
        register_sidebar( array(
            'name'          => __( 'Thanh dưới bài viết', 'vietnamcoder' ),
            'id'            => 'sidebar-2',
            'description'   => __( 'Thêm widgets ở đây sẽ xuất hiện phần dưới cùng bài viết', 'vietnamcoder' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s footer-column">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        ) );
    }
}

