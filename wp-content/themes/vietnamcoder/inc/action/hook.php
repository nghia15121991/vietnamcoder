<?php
/**
 * Danh sách các hook
 *
 *
 *
 * @package WordPress
 * @subpackage vietnamcoder
 * @since 1.0
 * @version 1.0
 */


/**
 * Thêm CSS JS
 */
add_action("wp_enqueue_scripts", "aego_themes_css");
add_action("wp_enqueue_scripts", "aego_themes_js");
add_action( 'wp_footer', 'my_deregister_scripts' );

//Tạo các sider bar widget
add_action( 'widgets_init', 'initialize_widget' );

//Phần dành cho wpDiscuz 
     