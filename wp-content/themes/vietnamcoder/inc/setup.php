<?php
/**
 * Setup các tính năng của wordpress
 *
 *
 *
 * @package WordPress
 * @subpackage vietnamcoder
 * @since 1.0
 * @version 1.0
 */
define('DS', DIRECTORY_SEPARATOR);

//Kích hoạt các tính năng hỗ trợ cần thiết
add_theme_support( 'post-thumbnails' );
add_theme_support( 'menus' );
add_theme_support('post-formats',
    array(
        'image',
        'video',
        'aside',
        'gallery',
        'quote',
        'link'
    )
);
add_theme_support('custom-logo');
//Thêm tính năng excerpt cho post_type questions
add_post_type_support( 'page', 'excerpt' );

add_image_size( 'vnc-image-small', 60, 60, true );
//Xóa thẻ p 2 đầu

remove_filter( 'the_excerpt', 'wpautop' );

/**
 * Đăng ký vị trí của menu
 */
register_nav_menus(
    array(
        // location => description
        'main-header' => 'Phần chính',
        'bottom-header-info' => 'Thông tin liên hệ'
    )
);
