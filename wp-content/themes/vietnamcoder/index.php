<?php
get_header();?>
        <div class="dn-container slide-section-phone">
            <div class="dn-flex dn-wrap-full container-box">
            <?php 
                if (have_posts()):
                    while(have_posts()): the_post()?>     
                        <div class="dn-box width-2">
                            <div class="top-box">
                                <div class="img-box">
                                    <img src="wp-content/themes/vietnamcoder/assets/myself/images/javascript.png" alt="">
                                </div>
                            </div>
                            <div class="middle-box">
                                <div class="title-box">
                                    <div>
                                        <h2><?php the_title('<span>','</span>');?></h2> 
                                    </div>
                                    <div class="time-box">
                                        <?php echo dn_posted_meta(); ?>
                                    </div>
                                </div>
                                <div class="content-box">
                                    <?php the_excerpt(); ?>
                                </div>
                                    <?php echo dn_posted_tag(); ?>
                            </div>
                            <div class="bot-box">
                                <div class="detail-box">
                                    <a href="<?php the_permalink();?>">Xem thêm</a>
                                </div>
                                
                            </div>
                        </div>
            <?php
                    endwhile;
                else:
                    echo "Hiện tại chưa có bài viết nào" 
            ?>
            <?php 
                endif;
            ?>

            </div>
        </div>
<?php 
get_footer();