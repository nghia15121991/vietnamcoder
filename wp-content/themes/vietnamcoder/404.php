<?php
/**
 * The template for displaying 404 pages (not found)
 *
 *
 * @package WordPress
 * @subpackage vietnamcoder
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="notfound">
	<div class="notfound">
		<div class="notfound-404"></div>
		<h1>404</h1>
		<h2>Oops! Page Not Be Found</h2>
		<p>Sorry but the page you are looking for does not exist, have been removed. name changed or is temporarily unavailable</p>
		<a href="<?php echo home_url( '/' )?>">Back to homepage</a>
	</div>
</div>

<?php get_footer();
