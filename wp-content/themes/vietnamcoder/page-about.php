<?php
get_header();?>

        <div class="dn-container dn-row">
            <div class="dn-row page-about">
                <div class="intro-name">
                    <div class="image-name">
                        <img src="https://via.placeholder.com/300" alt="">
                    </div>
                    <div class="content-name">
                        <h3>Xin chào,</h3>
                        <h3>Tôi tên là Nghĩa</h3>
                        <span>
                            Tôi là một lập trình viên mới ra trường chưa thật sự có nhiều kinh nghiệm làm việc trong môi trường lập trình, 
                            nhưng trong quá trình học ở trường tôi luôn được trau đồi những kiến thức nền tảng cơ bản về lập trình thông qua những môn học và các project nhỏ. 
                            Ngoài ra, tôi còn tích cực tham gia các hoạt động ngoại khóa, công tác Đoàn trong hai năm đầu , 
                            các công việc bán thời gian cho sinh viên để tăng cường các kỹ năng giao tiếp cộng đồng, kỹ năng ứng xử với bạn bè, đồng nghiệp, cấp trên,...
                        </span>
                    </div>
                    
                </div>
                <div class="exp-jobs">
                    <h3>Kinh nghiệm làm việc</h3>
                    <div>
                        <ul>
                            <li>
                                <span class="circle"></span>
                                <div class="positon-job">Student</div>
                                <div class="company-job">Trường Đại học Thủ Dầu Một</div>
                                <div class="content-job">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                    It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
                                </div>
                                <span class="time-job">
                                    <span>09/2014</span>
                                    <span>07/2018</span>
                                </span>
                                
                            </li>
                            <li>
                                <span class="circle"></span>
                                <div class="positon-job">Fresher</div>
                                <div class="company-job">Janeto</div>
                                <div class="content-job">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                    It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
                                </div>
                                <span class="time-job">
                                    <span>06/2017</span>
                                    <span>09/2017</span>
                                </span>
                            </li>
                            <li>
                                <span class="circle"></span>
                                <div class="positon-job">Back-End Developer</div>
                                <div class="company-job">Aegona</div>
                                <div class="content-job">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                </div>
                                <span class="time-job">
                                    <span>07/2018</span>
                                    <span>Bây giờ</span>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="skill-jobs">
                    <h3>Kỹ năng</h3>
                    <div class="skill-technical">
                        <div class="skill-container">
                            <span class="name-skill">CSS</span>
                            <div class="skills css">
                                <span>80%</span> 
                            </div>
                        </div>
                        <div class="skill-container">
                            <span class="name-skill">JS</span>
                            <div class="skills js">
                                <span>70%</span> 
                            </div>
                        </div>
                        <div class="skill-container">
                            <span class="name-skill">PHP</span>
                            <div class="skills php">
                                <span>75%</span> 
                            </div>
                        </div>
                        <div class="skill-container">
                            <span class="name-skill">NodeJS</span>
                            <div class="skills nodejs">
                                <span>65%</span> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contact-jobs">
                    <h3>Liên hệ</h3>
                    <div class="form-about">
                        <form action="" method="post" id="about-form">
                            <div class="input-required">
                                <input type="text" class ="form-input" name="dn_name" id="" placeholder="Tên (*)">
                                <input type="text" class ="form-input" name="dn_phone" id="" placeholder="Số điện thoại (*)">
                                <input type="text" class ="form-input" name="dn_email" id="" placeholder ="Email (*)">
                            </div>
                            <div class="input-unrequired">
                                <textarea name ="dn_message" class="form-textarea" placeholder ="Nội dung cần gửi"></textarea>
                            </div>
                            <input type="submit" value="Gửi" class ="form-submit" >
                        </form>
                    </div>
                </div>
            </div>
        </div>

<?php 
get_footer();