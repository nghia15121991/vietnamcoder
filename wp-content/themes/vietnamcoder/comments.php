<?php
/**
 * Phần hiển thị comment, cho phép người dùng gửi bình luận và xem bình luận của người khác
 *
 *
 * @package WordPress
 * @subpackage vietnamcoder
 * @since 1.0
 * @version 1.0
 */

if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area content-left">

    <?php
    comment_form();
    ?>

</div><!-- #comments -->
