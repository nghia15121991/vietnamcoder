<?php
get_header();?>

        <div class="dn-container">
            <div class="dn-flex dn-wrap-full container-box">
            <?php 
                if (have_posts()):
                    while(have_posts()): the_post()?>     
                        <div class="dn-box width-2">
                            <div class="top-box">
                                <div class="img-box">
                                    <img src="wp-content/themes/vietnamcoder/assets/myself/images/javascript.png" alt="">
                                </div>
                            </div>
                            <div class="middle-box">
                                <div class="title-box">
                                    <h2><?php the_title();?></h2>
                                </div>
                                <div class="content-box">
                                    <?php the_excerpt(); ?>
                                </div>
                                <div class="tag-box">
                                    <a href="#">Lập trình</a>
                                    <a href="#">JavaScript</a>
                                    <a href="#">Căn bản</a>
                                </div>
                            </div>
                            <div class="bot-box">
                                <div class="detail-box">
                                    <a href="#">Xem thêm</a>
                                </div>
                            </div>
                        </div>
            <?php
                    endwhile;
                else:
                    echo "Không tồn tại bài viết nào" 
            ?>
            <?php 
                endif;
            ?>

            </div>
        </div>

<?php 
get_footer();