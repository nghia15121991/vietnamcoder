<?php
/**
 * Trang đầu tiên vào website
 *
 *
 *
 * @package WordPress
 * @subpackage vietnamcoder
 * @since 1.0
 * @version 1.0
 */

get_header();?>
        <div class="dn-container slide-section-phone">
            <div class="dn-flex dn-wrap-full container-box">
            <?php 
                $paged = (get_query_var('page')) ? get_query_var('page') : 1;
                $query = array(
                    'post_type'     =>  'post',
                    'post_status'   =>  'publish',
                    'posts_per_page' => 6,
                    'paged' => $paged
                );
                $posts = new WP_Query($query);
                if ($posts->have_posts()):
                    while($posts->have_posts()): $posts->the_post()?>     
                        <div class="dn-box width-3">
                            <div class="top-box">
                                <div class="img-box">
                                    <?php the_post_thumbnail('medium')?>
                                </div>
                            </div>
                            <div class="middle-box">
                                <div>
                                    <div class="title-box">
                                        <a href="<?php the_permalink();?>"><?php the_title('<h3>','</h3>');?></a>
                                    </div>
                                    <div class="time-box">
                                        <?php echo dn_posted_meta(); ?>
                                        <span class="entry-comment"><?php comments_popup_link( __('0 Bình luận','vietnamcoder'), __('1 Bình luận', 'vietnamcoder'), __('% Bình luận', 'vietnamcoder'), 'comments-link', __('Tắt Bình luận', 'vietnamcoder'));?></span>
                                    </div>
                                </div>
                                <div class="content-box">
                                    <?php the_excerpt(); ?>
                                </div>
                                    <?php echo dn_posted_tag(); ?>
                            </div>
                            <div class="bot-box">
                                <div class="detail-box">
                                    <a href="<?php the_permalink();?>">Xem thêm</a>
                                </div>
                                
                            </div>
                        </div>
            <?php
                    endwhile;
                        
                else:
                    echo "Hiện tại chưa có bài viết nào"; 
            ?>
                
            <?php 
                endif;?>
            </div>
            <div class="pagination-page">
                <?php
                    next_posts_link( '<span class="yotu-pagination-next yotu-button-prs yotu-button-prs-3">Cũ hơn</span>', $posts ->max_num_pages);
                    previous_posts_link( '<span class="yotu-pagination-next yotu-button-prs yotu-button-prs-3">Mới hơn</span>' );
                ?>
            </div>
            
        </div>
        <div class="ytb-video">
            <div class="dn-container ytb-video-container">
                <h2 class="ytb-video-title">Kênh Youtube</h3>
                <h3 class="ytb-video-des">Xem ngay các video và ủng hộ cho kênh luôn phát triển</h3>
                <div class="ytb-video-content">
                    <?php echo do_shortcode('[yotuwp type="channel" id="UCCL8eEQRMWv4JiKu6588FYg" ]'); ?>
                </div>
            </div>
        </div>
        
<?php 
get_footer();